package com.mdw.xxl.job.task;

import com.alibaba.fastjson.JSON;
import com.mdw.xxl.job.task.config.XxlJobAutoConfiguration;
import com.mdw.xxl.job.task.config.XxlJobProperties;
import com.mdw.xxl.job.task.request.ExecutorAddRequest;
import com.mdw.xxl.job.task.request.ExecutorGetRequest;
import com.mdw.xxl.job.task.request.ExecutorRegistryRequest;
import com.mdw.xxl.job.task.request.JobAddRequest;
import com.mdw.xxl.job.task.request.JobStartRequest;
import com.mdw.xxl.job.task.request.JobStopRequest;
import com.mdw.xxl.job.task.request.JobUpdateRequest;
import com.mdw.xxl.job.task.response.ExecutorGetResponse;
import com.mdw.xxl.job.task.util.IpUtil;
import com.mdw.xxl.job.task.util.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author madingwen
 * @version $Id: XxlJobTest.java, v 0.1 2022-04-29 15:03 mdw Exp $$
 */
@Slf4j
@SpringBootTest(classes = XxlJobAutoConfiguration.class)
@RunWith(SpringRunner.class)
public class XxlJobTest {

    @Autowired
    private XxlJobProperties xxlJobProperties;
    @Autowired
    private XxlJobClient xxlJobClient;

    // 执行器id：提前在xxl-job后台配置好
    private static final Integer jobGroup = 3;
    // 执行任务id
    private static final Integer jobId = 10;

    @Test
    public void testAddJob() {
        String executorHandler = "checkAccountTask";
        String author = "马丁丁";

        JobAddRequest request = new JobAddRequest();
        request.setJobGroup(jobGroup)
                .setJobDesc("商户-微信对账")
                .setJobCron("0 0 7 * * ?")
                .setExecutorHandler(executorHandler)
                .setExecutorParam("paycenter-wx")
                .setAuthor(author);
        String result = xxlJobClient.execute(request);
        log.info(result);

        JobAddRequest request2 = new JobAddRequest();
        request2.setJobGroup(jobGroup)
                .setJobDesc("商户-支付宝对账")
                .setJobCron("0 0 4 * * ?")
                .setExecutorHandler(executorHandler)
                .setExecutorParam("paycenter-zfb")
                .setAuthor(author);
        String result2 = xxlJobClient.execute(request);
        log.info(result2);
    }

    @Test
    public void testUpdateJob() {
        JobUpdateRequest request = new JobUpdateRequest();
        request.setId(jobId)
                .setJobGroup(jobGroup)
                .setJobCron("0 0 0 1 1 ? *")
                .setJobDesc("支付-微信对账")
                .setExecutorHandler("checkAccountTask")
                .setExecutorParam("paycenter-wx")
                .setAuthor("马丁丁");
        String result = xxlJobClient.execute(request);
        log.info(result);
    }

    @Test
    public void testStopJob() {
        JobStopRequest request = new JobStopRequest();
        request.setId(jobId);
        String result = xxlJobClient.execute(request);
        log.info(result);
    }

    @Test
    public void testStartJob() {
        JobStartRequest request = new JobStartRequest();
        request.setId(jobId);
        String result = xxlJobClient.execute(request);
        log.info(result);
    }

    @Test
    public void testGetExecutor() {
        ExecutorGetRequest getRequest = new ExecutorGetRequest();
        getRequest.setAppname(xxlJobProperties.getAppname());
        getRequest.setStart(0);
        getRequest.setLength(1);
        ExecutorGetResponse getResponse = xxlJobClient.execute(getRequest);
        log.info(JSON.toJSONString(getResponse));
    }

    @Test
    public void testAddExecutor() {
        ExecutorAddRequest addRequest = new ExecutorAddRequest();
        addRequest.setTitle("对账中心执行器2");
        addRequest.setAppname(xxlJobProperties.getAppname());
        String addResponse = xxlJobClient.execute(addRequest);
        log.info("addResponse：{}", JSON.toJSONString(addResponse));

        String ip = (xxlJobProperties.getIp() != null && xxlJobProperties.getIp().trim().length() > 0) ? xxlJobProperties.getIp() : IpUtil.getIp();
        int port = xxlJobProperties.getPort() > 0 ? xxlJobProperties.getPort() : NetUtil.findAvailablePort(9999);

        ExecutorRegistryRequest registryRequest = new ExecutorRegistryRequest();
        // 需和xxl-job-admin配置的accessToken保持一致
        registryRequest.setAccessToken(xxlJobProperties.getAccessToken());
        registryRequest.setRegistryKey(xxlJobProperties.getAppname());
        registryRequest.setRegistryValue(ip, port);
        String registryResponse = xxlJobClient.execute(registryRequest);
        log.info("registryResponse：{}", JSON.toJSONString(registryResponse));
    }
}