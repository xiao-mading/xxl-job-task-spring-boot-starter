package com.mdw.xxl.job.task.request;

import lombok.Data;

/**
 * 启动执行任务
 *
 * @author madingwen
 * @version $Id: JobStopRequest.java, v 0.1 2022-05-05 18:49 mdw Exp $$
 */
@Data
public class JobStartRequest extends BaseXxlJobRequest<String> {

    /**
     * 任务id
     */
    private Integer id;

    /**
     * 请求路径
     *
     * @return
     */
    @Override
    public String getUrl() {
        return "/jobinfo/start";
    }

    /**
     * 返回结果的类型
     *
     * @return
     */
    @Override
    public Class<String> getResponseClass() {
        return String.class;
    }
}