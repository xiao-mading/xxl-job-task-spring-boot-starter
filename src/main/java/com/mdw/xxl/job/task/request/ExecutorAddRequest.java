package com.mdw.xxl.job.task.request;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <pre>
 * 添加执行器
 *
 * 说明：
 *   添加完执行器后，若实时性要求不高，无需显示调用注册接口(/api/registry)，客户端会自动定时注册
 *   客户端自动注册任务(每30s注册1次)：{@link com.xxl.job.core.thread.ExecutorRegistryThread}
 * </pre>
 *
 * @author madingwen
 * @version $Id: ExecutorAddRequest.java, v 0.1 2022-05-03 23:37 mdw Exp $$
 */
@Accessors(chain = true)
@Data
public class ExecutorAddRequest extends BaseXxlJobRequest<String> {

    private String appname;
    private String title;
    /**
     * 执行器地址类型：0=自动注册、1=手动录入
     */
    private int addressType = 0;

    /**
     * 请求路径
     *
     * @return
     */
    @Override
    public String getUrl() {
        return "/jobgroup/save";
    }

    /**
     * 返回结果的类型
     *
     * @return
     */
    @Override
    public Class<String> getResponseClass() {
        return String.class;
    }
}