package com.mdw.xxl.job.task.config;

import com.mdw.xxl.job.task.DefaultXxlJobClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author madingwen
 * @version $Id: XxljobTaskAutoConfiguration.java, v 0.1 2022-04-29 10:59 mdw Exp $$
 */
@Configuration
@EnableConfigurationProperties(XxlJobProperties.class)
public class XxlJobAutoConfiguration {

    @Bean("defaultXxlJobClient")
    public DefaultXxlJobClient defaultXxlJobClient() {
        return new DefaultXxlJobClient();
    }
}