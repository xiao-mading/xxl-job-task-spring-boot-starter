<p align="center">
	<strong>开箱即用的xxl-job任务后台动态管理</strong>
</p>


# 背景

由于最近要设计开发一个通用对账系统，不同的对账参与方的对账节点节点不一

- [ ] 例如：

- 资金对账：

  ```shell
  微信：可能是每天7点以后，才能下载前1天的对账单
  通联：可能是每天8点以后，才能下载前1天的对账单
  ```

- 业务对账：不同的业务动作，需灵活配置对账时间节点

所以采用了xxl-job来实现此功能，但不可能每接入一个对账业务方，都去xxl-job后台手动建1个任务。

最终采用如下方案：所有对账任务采用同1个执行器，不同的对账场景使用不同的执行任务，并以任务参数进行区分，使用`xxl-job-admin`暴露的后台`API`动态进行任务添加、修改、启动、停止等功能。这样当新接入1个对账业务方，只需要在新加配置时，在后台动态新增一个xxl-job执行任务，就可以实现对账调度

![Snipaste_2022-05-06_16-32-43](images/Snipaste_2022-05-06_16-32-43.png) 

□ xxl-job后台效果：

<img src="images/Snipaste_2023-08-07_15-51-06.png" alt="Snipaste_2022-05-06_16-32-43" style="zoom:62%;" /> 

<img src="images/Snipaste_2023-08-07_15-56-32.png" alt="Snipaste_2022-05-06_16-32-43" style="zoom:65%;" /> 

在网上搜索，也有比较多的博客涉及到如何访问xxl-job-admin的接口，但要么代码不全，要么缺乏扩展性，为了接入简单，开箱即用，封装了一个starter，帮助有同样需求的同学快速投入项目使用

# 功能
- xxl-job模拟登录
- xxl-job执行器：动态新增、动态注册、动态查询
- xxl-job执行任务：动态添加、动态更新、动态启动、动态停止

# 特性
- xxl-job-admin的API需要登录才能访问，本项目中进行了模拟登录，获取到cookie，然后携带cookie访问API
- 对接口访问进行统一封装，扩展性强
- 将工程封装成spring-boot-starter，开箱即用
- 本项目对接的xxl-job接口，都进行了单元测试，单元测试案例见`com.chuanxi.wow.xxl.job.task#XxlJobTest`


# 如何使用

1、下载源码

2、打成jar，放入maven仓库

3、引用

```xml
<!--例如：-->
<dependency>
    <groupId>com.mading</groupId>
    <artifactId>xxl-job-task-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

4、配置xxl-job

```properties
# 前缀可自行在XxlJobProperties中修改
xxl.adminAddresses=http://127.0.0.1:8080/xxl-job-admin
xxl.appname=${spring.application.name}
xxl.logretentiondays=30
xxl.port=9999
xxl.accessToken=289d5e0b-e83b-40a1-87ec-fd7b2335f15c
```

5、使用

```java
package com.mdw.xxl.job.task;

@Slf4j
@SpringBootTest(classes = XxlJobAutoConfiguration.class)
@RunWith(SpringRunner.class)
public class XxlJobTest {

    @Autowired
    private XxlJobProperties xxlJobProperties;
    @Autowired
    private XxlJobClient xxlJobClient;

    // 执行器id
    private static final Integer jobGroup = 3;
    // 执行任务id
    private static final Integer jobId = 10;

    @Test
    public void testAddJob() {
        String executorHandler = "checkAccountTask";
        String author = "马丁丁";

        JobAddRequest request = new JobAddRequest();
        request.setJobGroup(jobGroup)
                .setJobDesc("商户-微信对账")
                .setJobCron("0 0 7 * * ?")
                .setExecutorHandler(executorHandler)
                .setExecutorParam("paycenter-wx")
                .setAuthor(author);
        String result = xxlJobClient.execute(request);
        log.info(result);

        JobAddRequest request2 = new JobAddRequest();
        request2.setJobGroup(jobGroup)
                .setJobDesc("商户-支付宝对账")
                .setJobCron("0 0 4 * * ?")
                .setExecutorHandler(executorHandler)
                .setExecutorParam("paycenter-zfb")
                .setAuthor(author);
        String result2 = xxlJobClient.execute(request);
        log.info(result2);
    }
}
```





# 参考

xxl-job动态添加执行任务：`https://blog.csdn.net/niugang0920/article/details/117601547`
